//! Model for accessing submission information.

use serde::{Deserialize, Serialize};

use crate::canvas::CanvasInformation;
use crate::models::prelude::*;
use crate::parameters::*;
use crate::requests::*;

#[derive(Debug, Deserialize, Serialize)]
pub struct SubmissionAttachment {
    pub id: usize,
    pub uuid: String,
    pub folder_id: Option<usize>,
    pub display_name: String,
    pub filename: String,
    pub upload_status: String,
    #[serde(rename = "content-type")]
    pub content_type: String,
    pub url: String,
    pub size: usize,
    pub created_at: String,
    pub updated_at: String,
    pub unlock_at: Option<String>,
    pub locked: bool,
    pub hidden: bool,
    pub lock_at: Option<String>,
    pub hidden_for_user: bool,
    pub thumbnail_url: Option<String>,
    pub modified_at: String,
    pub mime_class: Option<String>,
    pub media_entry_id: Option<usize>,
    pub locked_for_user: bool,
    pub preview_url: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Submission {
    pub assignment_id: Option<usize>,
    pub assignment: Option<usize>,
    pub course: Option<usize>,
    pub attempt: Option<usize>,
    pub body: Option<String>,
    pub grade: Option<String>,
    pub grade_matches_current_submission: Option<bool>,
    pub html_url: Option<String>,
    pub preview_url: Option<String>,
    pub score: Option<f64>,
    pub submission_comments: Option<Vec<String>>,
    pub submission_type: Option<String>,
    pub submitted_at: Option<String>,
    pub url: Option<String>,
    pub user_id: Option<usize>,
    pub grader_id: Option<usize>,
    pub graded_at: Option<String>,
    pub user: Option<usize>,
    pub late: bool,
    pub assignment_visible: Option<bool>,
    pub excused: Option<bool>,
    pub missing: Option<bool>,
    pub late_policy_status: Option<String>,
    pub points_deducted: Option<f64>,
    pub seconds_late: Option<usize>,
    pub workflow_state: Option<String>,
    pub extra_attempts: Option<usize>,
    pub anonymous_id: Option<String>,
    pub posted_at: Option<String>,
    pub attachments: Option<Vec<SubmissionAttachment>>,
}
